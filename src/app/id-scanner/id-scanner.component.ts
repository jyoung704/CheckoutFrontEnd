import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {DelayService} from '../services/delay-service/delay.service';
import {NewGuestService} from '../services/new-guest-service/new-guest.service';

@Component({
  selector: 'app-id-scanner',
  templateUrl: './id-scanner.component.html',
  styleUrls: ['./id-scanner.component.css'],
  providers: [DelayService]
})
export class IdScannerComponent implements OnInit {
  inputValue: string;
  studentId: string;
  rawScannerInput: string;
  isParsingScannerInput: boolean = false;

  public SCANNER_TRIGGER_KEY = ';';
  public SCANNER_START_PATTERN: string = '0000';
  public SCANNER_END_KEY: string = 'Shift';

  idTemplate = new FormControl('', [Validators.required, Validators.minLength(7), Validators.maxLength(7), Validators.pattern(/^['0-9']*$/)]);

  constructor(private delayService: DelayService, public newGuestService: NewGuestService) {
  }

  ngOnInit() {
    this.delayService.delay(150).then(() => {
      this.focusOnTextEntry();
    });
  }

  @ViewChild('focusMe') nameField: ElementRef;

  focusOnTextEntry(): void {
    this.nameField.nativeElement.focus();
  }

  getErrorMessage() {
    if (this.idTemplate.hasError('minlength')) {
      return 'This ID is too short';
    }
    if (this.idTemplate.hasError('maxlength')) {
      return 'This ID is too long';
    }
    if (this.idTemplate.hasError('pattern')) {
      return 'Only numbers are allowed';
    }
    return this.idTemplate.hasError('required') ? 'You must enter a value' : '';
  }

  @HostListener('window:keyup', ['$event'])
  public onKey($event: KeyboardEvent) {
    const key = $event.key;
    console.log('key is: ' + key);
    if (key == this.SCANNER_TRIGGER_KEY) {
      this.resetValuesForNewInput();
      this.isParsingScannerInput = true;
    } else if (key == this.SCANNER_END_KEY) {
      this.parseStudentId(this.rawScannerInput);
      this.isParsingScannerInput = false;
    } else if (key == 'Escape') {
      this.isParsingScannerInput = false;
    } else if (this.isParsingScannerInput == true) {
      this.rawScannerInput += key;
    }

    // Cancel parsing if beginning of input doesn't match start pattern
    if (this.rawScannerInput.length <= this.SCANNER_START_PATTERN.length) {
      this.verifyScanPattern(this.rawScannerInput);
    }
  }

  /**
   * Stops parsing the input if the the input pattern doesn't match the expected starting pattern
   * @param rawScannerInput
   */
  verifyScanPattern(rawScannerInput: string) {
    const expectedStartingPattern = this.SCANNER_START_PATTERN.substring(0, rawScannerInput.length);
    if (expectedStartingPattern != rawScannerInput) {
      this.resetValuesForNewInput();
      this.isParsingScannerInput = false;
    }
  }

  resetValuesForNewInput() {
    this.studentId = '';
    this.inputValue = '';
    this.rawScannerInput = '';
  }

  onSubmitUserId() {
    this.studentId = this.inputValue;
    this.newGuestService.checkGuestRegistration(this.studentId);
  }

  parseStudentId(input: string) {
    // Remove 4 leading zeros and two trailing zeros
    const idStart = 4;
    const idLength = 7;
    const idEnd = idStart + idLength;
    this.studentId = input.substring(idStart, idEnd);
    this.inputValue = this.studentId;
  }

  public onInputKeyUpEnter() {
    if (!this.idTemplate.invalid) {
      console.log('enter was pressed');
      const inputStartPattern = this.inputValue.substring(0, 4);
      console.log(inputStartPattern);
      if (inputStartPattern == this.SCANNER_START_PATTERN) {
        this.parseStudentId(this.inputValue);
        this.inputValue = this.studentId;
      } else {
        this.onSubmitUserId();
      }
    }
  }

  public getId(): string {
    return this.studentId;
  }
}
