import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeightEntryComponent} from './weight-entry.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {HttpClientModule} from '@angular/common/http';

describe('WeightEntryComponent', () => {
  let component: WeightEntryComponent;
  let fixture: ComponentFixture<WeightEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WeightEntryComponent],
      imports: [MatSnackBarModule, HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
