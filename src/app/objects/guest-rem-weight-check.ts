export class GuestRemWeightCheck {
  studentID: string;
  startDate: string;
  endDate: string;


  constructor(studentID: string, startDate: string, endDate: string) {
    this.studentID = studentID;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  getStudentID(): string {
    return this.studentID;
  }

  getStartDate(): string {
    return this.startDate;
  }

  getEndDate(): string {
    return this.endDate;
  }
}
